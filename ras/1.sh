#!/bin/bash
mkdir ~/openvpn


cat <<EOT > ~/openvpn/client.conf
dev tun
proto udp
remote 192.168.10.10 1207
client
resolv-retry infinite
ca ./ca.crt
cert ./client.crt
key ./client.key
route 192.168.10.0 255.255.255.0
persist-key
persist-tun
comp-lzo
verb 3

EOT

echo -e "\n"|ssh-keygen -t rsa -N ""
sshpass -p 'vagrant' ssh-copy-id -o "StrictHostKeyChecking no" root@192.168.10.10

scp root@192.168.10.10:/etc/openvpn/pki/ca.crt ~/openvpn/ca.crt
scp root@192.168.10.10:/etc/openvpn/pki/issued/client.crt ~/openvpn/client.crt
scp root@192.168.10.10:/etc/openvpn/pki/private/client.key ~/openvpn/client.key


